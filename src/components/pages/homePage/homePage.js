import React, {Component} from 'react';
import ItemList from '../../itemList';
import ItemDetails, {Field} from '../../itemDetails';
import ErrorMessage from '../../errorMessage';
import gotService from '../../../services/gotService';
import './homePage.css';

export default class HomePage extends Component {
    gotService = new gotService();

    state = {
        selectedHouse: null,
        error: false
    }

    onItemSelected = (id) => {
        this.setState({
            selectedHouse: id
        })
    }

    componentDidCatch() {
        this.setState({
            error: true
        })
    }

    render() {
        if (this.state.error) {
            return <ErrorMessage/>
        }

        return (
            <p className="lightn-text">
                Game of Thrones is an American fantasy drama television series created by David Benioff and D. B. 
                Weiss for HBO. It is an adaptation of A Song of Ice and Fire, George R. R. Martin's series of fantasy 
                novels, the first of which is A Game of Thrones (1996). The show was both produced and filmed in Belfast 
                and elsewhere in the United Kingdom. Filming locations also included Canada, Croatia, Iceland, Malta, Morocco, 
                and Spain. The series premiered on HBO in the United States on April 17, 2011, and concluded on May 19, 2019, with
                73 episodes broadcast over eight seasons.
            </p>
        )
    }
}
