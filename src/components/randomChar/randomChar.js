import React, {Component} from 'react';
import PropTypes from 'prop-types'
import './randomChar.css';
import Spiner from '../spiner';
import ErrorMessage from '../errorMessage';

import gotServise from '../../services/gotService';


export default class RandomChar extends Component {

    gotServise = new gotServise();
    state = {
        char: {},
        loading: true,
        error: false
    }

    componentDidMount() {
        this.updateChar();
        this.timerId = setInterval(this.updateChar, this.props.interval);
    }

    componentWillUnmount() {
        clearInterval(this.timerId);
    }

    onCharLoaded = (char) => {
        this.setState({
            char,
            loading: false
        });
    }

    onEror = (err) => {
        this.setState({
            error: true,
            loading: false
        })
    }

    updateChar = () => {        
        const id = Math.floor(Math.random()*140 +25); //25-140
        this.gotServise.getCharacter(id)
            .then(this.onCharLoaded)
            .catch(this.onEror)

    }

    render() {
        const {char, loading, error} = this.state;
        const errorMessage = error ? <ErrorMessage /> : null;
        const spiner = loading ? <Spiner/> : null
        const content = !(loading || error) ? <View char={char}/> : null;
        return (

            <div className="random-block rounded">
                {errorMessage}
                {spiner}
                {content}
            </div>
        );
    }
}

RandomChar.defaultProps = {
    interval: 10000
}

RandomChar.propTypes = {
    interval: PropTypes.number
}

// RandomChar.propTypes = {
//     interval: (props, propName, componentName) => {
//         const value = props[propName];

//         if(typeof value === 'number' && !isNaN(value)) {
//             return null
//         } 
//         return new TypeError(`${componentName}: ${propName} must be a number`)
//     }
// }

const View = ({char}) => {
    const {name, gender, born, died, culture} = char;
    return(
        <>
            <h4>Random Character: {name}</h4>
            <ul className="list-group list-group-flush">
                <li className="list-group-item d-flex justify-content-between">
                    <span className="term">Gender </span>
                    <span>{gender}</span>
                </li>
                <li className="list-group-item d-flex justify-content-between">
                    <span className="term">Born </span>
                    <span>{born}</span>
                </li>
                <li className="list-group-item d-flex justify-content-between">
                    <span className="term">Died </span>
                    <span>{died}</span>
                </li>
                <li className="list-group-item d-flex justify-content-between">
                    <span className="term">Culture </span>
                    <span>{culture}</span>
                </li>
            </ul>
        </>
    )
}