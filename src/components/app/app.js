import React, {Component} from 'react';
import {Col, Row, Container} from 'reactstrap';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Header from '../header';
import RandomChar from '../randomChar';
import CharacterPage from '../pages/characterPage';
import BooksPage from '../pages/booksPage';
import HousesPage from '../pages/housesPage';
import HomePage from '../pages/homePage';
import NoMatch from '../noMatch'
import BooksItem from '../pages/booksItem';


import ErrorMessage from '../errorMessage';
import gotService from '../../services/gotService';

import './app.css'

export default class App extends Component {

    gotService = new gotService();

    state = {
        showRandomChar: true,
        error: false,
        selectedChar: 130
    }

    componentDidCatch() {
        this.setState({
            error: true
        })
        
    }

    toggleRandomChar = () => {
        this.setState((state) => {
            return {
                showRandomChar: !state.showRandomChar
            }
        });
    }



    onItemSelected = (id) => {
        this.setState({
            selectedChar: id
        })
    }


    render() {
        if (this.state.error) {
            return <ErrorMessage/>
        }
        const char = this.state.showRandomChar ? <RandomChar/> : null;
        return (
            <Router>
                <div className="app"> 
                    <Container>
                        <Header />
                    </Container>
                    <Container>
                        <Row>
                            <Col lg={{size: 5, offset: 0}}>
                                {char}
                                <button 
                                    className="toggle-btn"
                                    onClick={this.toggleRandomChar}>Toggle random character</button>
                            </Col>
                        </Row>
                        <Switch>
                            <Route path="/" exact component={HomePage} />
                            <Route path="/characters/" component={CharacterPage} />
                            <Route path="/houses/" component={HousesPage} />
                            <Route path="/books/" exact component={BooksPage} />
                            <Route path="/books/:id" render={
                                ({match}) => {
                                    const {id} = match.params;
                                    return <BooksItem bookId={id}/>}
                            } />
                            <Route path="*" component={NoMatch}/>

                        </Switch>
                    </Container>
                </div>
            </Router>
        );
    }
};
