import React, {Component} from 'react';
import './noMatch.css'

export default class NoMatch extends Component {

    render() {
        return (
            <p className="error-text">
                Error 404
            </p>
        )
    }
}
