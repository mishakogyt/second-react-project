import React from 'react';
import './spiner.css';

const Spiner = () => {
   return(
     <div className="loadingio-spinner-spin-rq76rh2a0m">
        <div className="ldio-s3omd8yvg5e">
            <div><div></div></div><div><div></div></div><div><div>
            </div></div><div><div></div></div><div><div></div></div>
            <div><div></div></div><div><div></div></div><div><div></div></div>
         </div>
      </div>
   )
}

export default Spiner;